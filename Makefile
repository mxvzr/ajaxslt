# Currently, this Makefile has only a "dist" target.

PACKAGE = ajaxslt
VERSION = 0.8

DIST_FILES = \
  dom.js \
  util.js \
  xmltoken.js \
  xpath.js \
  xslt.js \

DIST_NAME = $(PACKAGE)-$(VERSION)

dist: clobber $(DIST_FILES)
	cat $(DIST_FILES) > $(DIST_NAME).js
	yui-compressor $(DIST_NAME).js > $(DIST_NAME).min.js

clobber: